//Question:1

package com.example.demo;

public interface iBanking {


	public int depositAmount(int amount);
	public int withdrawAmount(int amount);
	

}

package com.example.demo;

public class Customer implements iBanking {
	
	private String customerName;
	private String depositType;
	private int baseAmount;
	
	Customer(String customerName, String depositType, int baseAmount){
		this.customerName = customerName;
		this.depositType = depositType;
		this.baseAmount = baseAmount;
	}
	
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDepositType() {
		return depositType;
	}
	public void setDepositType(String depositType) {
		this.depositType = depositType;
	}
	public int getBaseAmount() {
		return baseAmount;
	}
	public void setBaseAmount(int baseAmount) {
		this.baseAmount = baseAmount;
	}
	
	@Override
	public String toString() {
		return "Customer [customerName=" + customerName + ", depositType=" + depositType + ", baseAmount=" + baseAmount
				+ "]";
	}
	
	public void displayCustomerDetails() {
		System.out.println("Customer name: " +customerName);
		System.out.println("Deposit type: " +depositType);
		System.out.println("Base Amount: " +baseAmount);
	}

	@Override
	public int depositAmount(int amount) {
		// TODO Auto-generated method stub
		baseAmount = baseAmount + amount;
		return baseAmount;
	}
	@Override
	public int withdrawAmount(int amount) {
		// TODO Auto-generated method stub
		baseAmount = baseAmount - amount;
		return baseAmount;
	}
	
}

package com.example.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BankingCustomerProcess {

	public static void main(String args[]) {
		
		Customer customer[] = new Customer[5];
		customer[0] = new Customer("Melissa", "savings", 10000);
		customer[1] = new Customer("Grace", "fixed", 25000);
		customer[2] = new Customer("Jane", "current", 15500);
		customer[3] = new Customer("Robin", "Savings", 50000);
		customer[4] = new Customer("Austin", "current", 15000);
		
		for(int i=0; i<customer.length; i++) {
			System.out.printf("Details of Customer[%d]\n", i+1);
			customer[i].displayCustomerDetails();
		}
		
		System.out.println("do you want to withdraw or deposit?\n 1.Withdraw\n 2.Deposit");
		Scanner sc = new Scanner(System.in);
		int choice = sc.nextInt();
		if(choice == 1) {
			withdrawProcessing();
			for(int i=0;i<customer.length;i++) {
				System.out.printf("Enter the amount to be withdrawn for customer[%d] ", i+1);
				System.out.println("Current amount :" + customer[i].getBaseAmount());
				Scanner scanner = new Scanner(System.in);
				int amount = scanner.nextInt();
				customer[i].withdrawAmount(amount);
				System.out.println("Amount withdrawn is: " +amount);
				int balance = customer[i].getBaseAmount();
				System.out.println("Balance amount is: " +balance);
			}	
		}
		else if(choice == 2) {
			depositProcessing();
			for(int i=0;i<customer.length;i++) {
				System.out.printf("Enter the amount to be deposited for customer[%d] ", i+1);
				System.out.println("Current amount :" + customer[i].getBaseAmount());
				Scanner scanner = new Scanner(System.in);
				int amount = scanner.nextInt();
				customer[i].depositAmount(amount);
				System.out.println("Amount deposited is: " +amount);
				int balance = customer[i].getBaseAmount();
				System.out.println("Balance amount is: " +balance);
			}
		}
		sc.close();
	
	}
	
	public static void depositProcessing(){
		System.out.println("Deposit of amount is taking place.");
	}
	
	public static void withdrawProcessing() {
		System.out.println("Withdrawal of amount is taking place.");
	}
}

